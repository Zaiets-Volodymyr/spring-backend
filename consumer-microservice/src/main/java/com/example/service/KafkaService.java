package com.example.service;

import lombok.extern.slf4j.Slf4j;
import com.example.avro.UserSchema;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class KafkaService {

    private final String defaultTopic = "example";

    private final String usersTopic = "users";

    @KafkaListener(topics = defaultTopic, groupId = "example")
    public void listenGroupFoo(String message) {
        System.out.println("Received message from topic \"" + defaultTopic + "\": " + message);
    }

    @KafkaListener(topics = usersTopic, groupId = "users", containerFactory = "myConsumerFactory")
    public void consume(UserSchema message) {
        log.info("User: " + message.toString());
    }
}
