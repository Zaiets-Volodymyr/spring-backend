package com.example.service;

import com.example.gprc.user.UserRequest;
import com.example.gprc.user.UserResponse;
import com.example.gprc.user.UserServiceGrpc;
import com.example.payload.response.UserResp;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class GrpcService {

    private final UserServiceGrpc.UserServiceBlockingStub stub = grpcClient();

    @Bean
    public UserServiceGrpc.UserServiceBlockingStub grpcClient() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8091)
                .usePlaintext()
                .build();
        return UserServiceGrpc.newBlockingStub(channel);
    }

    public UserResp getUserByUuid(String uuid) {
        UserResponse userResponse = stub.getUserByUuid(UserRequest.newBuilder()
                .setUuid(uuid)
                .build());
        return new UserResp(userResponse.getUuid(),
                userResponse.getUsername(),
                userResponse.getEmail(),
                userResponse.getPassword());
    }

    public List<UserResp> getAllUsers() {
        Iterator<UserResponse> userResponses = stub.getAllUsers(UserRequest.newBuilder().build());
        List<UserResp> users = new ArrayList<>();

        while (userResponses.hasNext()) {
            UserResponse userResponse = userResponses.next();
            users.add(new UserResp(userResponse.getUuid(),
                    userResponse.getUsername(),
                    userResponse.getEmail(),
                    userResponse.getPassword()));
        }

        return users;
    }
}
