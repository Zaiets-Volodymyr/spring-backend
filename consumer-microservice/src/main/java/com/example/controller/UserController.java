package com.example.controller;

import com.example.service.GrpcService;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final GrpcService grpcService;

    @GetMapping("/user/{uuid}")
    public ResponseEntity<?> getUserByUuid(@PathVariable @NotNull String uuid) {
        return ResponseEntity.ok(grpcService.getUserByUuid(uuid));
    }

    @GetMapping("/users")
    public ResponseEntity<?> getAllUsers() {
        return ResponseEntity.ok(grpcService.getAllUsers());
    }
}
