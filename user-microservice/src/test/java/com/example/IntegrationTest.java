package com.example;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

@Testcontainers
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = {Application.class})
public class IntegrationTest {
    @LocalServerPort
    private int port;

    @Container
    static PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:14.1")
            .withDatabaseName("spring-backend")
            .withUsername("postgres")
            .withPassword("postgres")
            .withInitScript("db/test_init.sql");

    @DynamicPropertySource
    static void postgreSQLProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
        registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
        registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
    }

    @Autowired
    protected WebTestClient webTestClient;

    @BeforeEach
    void beforeEach() {
        RestAssured.baseURI = "http://localhost:" + port;
    }

    @Test
    public void getAllUsersWebTestClientTest() {
        webTestClient
                .get()
                .uri("/users")
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    public void getOneUserWebTestClientTest() {
        webTestClient
                .get()
                .uri("/user/{id}", 0)
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    public void getAllUsersTest() {
        given()
                .contentType(ContentType.JSON)
                .when()
                .get("/users")
                .then()
                .statusCode(200)
                .body(".", hasSize(2));
    }

    @Test
    public void getOneUserTest() {
        given()
                .contentType(ContentType.JSON)
                .when()
                .get("/user/0")
                .then()
                .statusCode(200)
                .body("username", equalTo("test"));
    }
}
