CREATE TABLE IF NOT EXISTS users
(
    id                      SERIAL        UNIQUE PRIMARY KEY,
    uuid                    VARCHAR(100)  NOT NULL  UNIQUE,
    username                VARCHAR(100)  NOT NULL,
    email                   VARCHAR(100)  NOT NULL,
    password                VARCHAR(100)  NOT NULL
);

DO
$do$
    BEGIN
        IF NOT EXISTS (SELECT FROM users) THEN
            INSERT INTO users(id, uuid, username, email, password)
            VALUES (0, '0', 'test', 'test', 'test');

            INSERT INTO users(id, uuid, username, email, password)
            VALUES (1, '1', 'username', 'email', 'password');
        END IF;
    END
$do$
