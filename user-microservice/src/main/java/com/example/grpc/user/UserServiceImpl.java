package com.example.grpc.user;

import com.example.entity.User;
import com.example.gprc.user.ErrorResponse;
import com.example.gprc.user.UserRequest;
import com.example.gprc.user.UserResponse;
import com.example.gprc.user.UserServiceGrpc;
import com.example.repository.UserRepository;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.protobuf.ProtoUtils;
import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class UserServiceImpl extends UserServiceGrpc.UserServiceImplBase {

    private final UserRepository userRepository;

    @Override
    public void getUserByUuid(UserRequest request, StreamObserver<UserResponse> responseObserver) {
        User user = userRepository.findByUuid(request.getUuid());

        if (user == null) {
            Metadata.Key<ErrorResponse> errorResponseKey = ProtoUtils.keyForProto(ErrorResponse.getDefaultInstance());
            ErrorResponse errorResponse = ErrorResponse.newBuilder()
                    .setActionError("User Not Found")
                    .setExpectedAction("Set the UUID of an existing user")
                    .build();
            Metadata metadata = new Metadata();
            metadata.put(errorResponseKey, errorResponse);
            responseObserver.onError(Status.NOT_FOUND
                    .withDescription("Can't find a user with that UUID")
                    .asRuntimeException(metadata));
        } else {
            UserResponse response = UserResponse.newBuilder()
                    .setUuid(user.getUuid())
                    .setUsername(user.getUsername())
                    .setEmail(user.getEmail())
                    .setPassword(user.getPassword())
                    .build();

            responseObserver.onNext(response);
            responseObserver.onCompleted();
        }
    }

    @Override
    public void getAllUsers(UserRequest request, StreamObserver<UserResponse> responseObserver) {
        List<User> users = userRepository.findAll();

        for (User user: users) {
            UserResponse response = UserResponse.newBuilder()
                    .setUuid(user.getUuid())
                    .setUsername(user.getUsername())
                    .setEmail(user.getEmail())
                    .setPassword(user.getPassword())
                    .build();
            responseObserver.onNext(response);
        }
        responseObserver.onCompleted();
    }
}
