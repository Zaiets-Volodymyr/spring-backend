package com.example.service;

import com.example.grpc.user.UserServiceImpl;
import com.example.repository.UserRepository;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Service
@AllArgsConstructor
public class GrpcService {

    private final UserRepository userRepository;

    @PostConstruct
    public void grpcServer() {
        Server server = ServerBuilder
                .forPort(8091)
                .addService(new UserServiceImpl(userRepository)).build();

        try {
            server.start();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
