package com.example.service;

import com.example.avro.UserSchema;
import com.example.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.stereotype.Service;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class KafkaService {

    private final String usersTopic = "users";

    private final KafkaTemplate<String, Object> kafkaTemplate = myKafkaTemplate();

    private final KafkaTemplate<String, String> simpleKafkaTemplate = simpleKafkaTemplate();

    @Bean
    private static KafkaTemplate<String, String> simpleKafkaTemplate() {
        Map<String, Object> props = new HashMap<>(new KafkaProperties().getProperties());
        props.put("bootstrap.servers", "localhost:9092");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        return new KafkaTemplate<>(new DefaultKafkaProducerFactory<>(props));
    }

    @Bean
    private static ProducerFactory<String, Object> producerProperties() {
        Map<String, Object> props = new HashMap<>(new KafkaProperties().getProperties());
        props.put("bootstrap.servers", "localhost:9092");
        props.put("schema.registry.url", "http://localhost:8085");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "io.confluent.kafka.serializers.KafkaAvroSerializer");
        props.put("SCHEMA", UserSchema.getClassSchema());
        return new DefaultKafkaProducerFactory<>(props);
    }

    @Bean
    public KafkaTemplate<String, Object> myKafkaTemplate() {
        KafkaTemplate<String, Object> myKafkaTemplate = new KafkaTemplate<>(producerProperties());
        myKafkaTemplate.setDefaultTopic(usersTopic);
        return myKafkaTemplate;
    }

    public void send(String topic, String message) {
        simpleKafkaTemplate.send(new ProducerRecord<>(topic, message));
        log.info("Message sent to the topic \"" + topic + "\" successfully");
    }

    public void send(User user) {
        UserSchema userSchema = new UserSchema(user.getUuid(), user.getUsername(), user.getEmail(), user.getPassword());
        kafkaTemplate.send(new ProducerRecord<>(usersTopic, userSchema));
        log.info("User sent to the topic \"" + usersTopic + "\" successfully");
    }
}
