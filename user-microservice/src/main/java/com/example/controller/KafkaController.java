package com.example.controller;

import com.example.service.KafkaService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/kafka")
public class KafkaController {

    private final KafkaService kafkaService;

    @GetMapping(value = "/producer")
    public String producer(@RequestParam("message") String message) {
        String topic = "example";
        kafkaService.send(topic, message);
        return "Message sent to the topic \"" + topic + "\" successfully";
    }
}
