package com.example.controller;

import com.example.entity.User;
import com.example.payload.request.UserRequest;
import com.example.service.UserService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.openapitools.api.UserApi;
import org.openapitools.model.UserDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class UserController implements UserApi {

    private final UserService userService;

    @PostMapping("/user")
    public ResponseEntity<?> addUser(@RequestBody UserRequest userRequest) {
        return ResponseEntity.ok(userService.add(userRequest));
    }

    @GetMapping("/user/{uuid}")
    public UserDto getUserByUuid(@PathVariable @NotNull String uuid) {
        return userService.getByUuid(uuid);
    }

    @DeleteMapping("/user/{uuid}")
    public ResponseEntity<?> deleteUser(@PathVariable @NotNull String uuid) {
        return ResponseEntity.ok(userService.deleteByUuid(uuid));
    }

    @PostMapping("/users")
    public ResponseEntity<?> addAllUsers(@RequestBody List<UserRequest> userRequests) {
        return ResponseEntity.ok(userService.addAll(userRequests));
    }

    @GetMapping("/users")
    public List<UserDto> getAllUsers() {
        return userService.getAll();
    }
}
