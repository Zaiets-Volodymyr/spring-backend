package com.example.mapper;

import com.example.entity.User;
import org.mapstruct.Mapper;
import org.openapitools.model.UserDto;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    List<UserDto> toDtos(List<User> entities);

    UserDto toDto(User entity);

}
